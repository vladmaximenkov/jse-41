package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}
