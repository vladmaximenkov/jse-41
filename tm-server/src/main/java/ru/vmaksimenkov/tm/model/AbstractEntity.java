package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity extends AbstractOwner {

    @ManyToOne
    private User user;

}
